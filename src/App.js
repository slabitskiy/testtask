import React, { Component } from "react";
import "./App.css";
import Span from "./components/Span";
import { AppContext } from "./context";
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from "./text.service";
import filterSynonymous from "./helpers/filterSynonymous";

class App extends Component {
  state = {
    nodes: [],
    selectedNodeIndex: null,
    nodesStyles: [],
    synonumous: [] // array of { id : the same with node, words }
  };

  getText() {
    getMockText().then(result => {
      const re = /[\s]/;
      const resultToArray = result.split(re);
      const nodes = resultToArray.map(word => (
        <Span onWordHighlighted={this.setHiglightedNode} word={word} />
      ));
      this.setState({ nodes });
    });
  }

  componentDidMount() {
    this.getText();
  }

  setHiglightedNode = async (index, word) => {
    const result = await fetch(
      `https://api.datamuse.com/words?rel_syn=${word}`
    );
    const synonumous = await result.json();

    this.setState(({ synonumous: prevSynonumous }) => ({
      selectedNodeIndex: index,
      synonumous: [
        ...prevSynonumous,
        { id: index, words: synonumous.map(synonum => synonum.word) }
      ]
    }));
  };

  stylingWordHandler = type => () => {
    const { selectedNodeIndex } = this.state;
    if (selectedNodeIndex) {
      this.setState(({ nodesStyles }) => {
        const nodeStyle = nodesStyles.find(
          node => node.id === selectedNodeIndex
        ) || {
          id: selectedNodeIndex,
          bold: type === "bold",
          underline: type === "underline",
          italic: type === "italic"
        };
        const index = nodesStyles.findIndex(
          node => node.id === selectedNodeIndex
        );
        const newNodesStyles = [...nodesStyles];

        if (index >= 0) {
          newNodesStyles.splice(index, 1, {
            ...nodeStyle,
            [type]: !nodeStyle[type]
          });
        } else {
          newNodesStyles.push(nodeStyle);
        }

        return {
          nodesStyles: newNodesStyles
        };
      });
    }
  };

  onReplaceSynonyms = word => {
    this.setState(
      ({ nodes: prevNodes, selectedNodeIndex, synonumous: prevSynonumous }) => {
        const newNodes = [...prevNodes];
        const newSynonumous = filterSynonymous(
          prevSynonumous,
          selectedNodeIndex
        );
        newNodes.splice(
          selectedNodeIndex,
          1,
          React.cloneElement(prevNodes[selectedNodeIndex], {
            word
          })
        );

        return {
          nodes: newNodes,
          selectedNodeIndex: null,
          synonumous: newSynonumous
        };
      }
    );
  };
  removeSynonyms = () => {
    this.setState(({ selectedNodeIndex, synonumous: prevSynonumous }) => {
      const newSynonumous = filterSynonymous(prevSynonumous, selectedNodeIndex);
      return {
        synonumous: newSynonumous
      };
    });
  };
  render() {
    const { nodes, nodesStyles, synonumous } = this.state;

    return (
      <div className="App">
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <AppContext.Provider
            value={{
              onReplaceSynonyms: this.onReplaceSynonyms,
              selectedNodeIndex: this.state.selectedNodeIndex,
              synonumous,
              removeSynonyms: this.removeSynonyms
            }}
          >
            <ControlPanel selectWordHandler={this.stylingWordHandler} />
            <FileZone nodes={nodes} styles={nodesStyles} />
          </AppContext.Provider>
        </main>
      </div>
    );
  }
}

export default App;
