import React, { Component } from "react";
import "./FileZone.css";

class FileZone extends Component {
  render() {
    return (
      <div id="file-zone">
        <div id="file">
          {this.props.nodes.map((el, index) =>
            React.cloneElement(el, {
              key: index,
              index,
              style:
                this.props.styles.find(nodeStyle => nodeStyle.id === index) ||
                {}
            })
          )}
        </div>
      </div>
    );
  }
}

export default FileZone;
