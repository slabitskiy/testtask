import React, { Component } from "react";
import onClickOutside from "react-onclickoutside";
import { AppContext } from "../context";

class Poppup extends Component {
  static contextType = AppContext;

  onReplaceSynonymsHandler = word => () => {
    const { onReplaceSynonyms } = this.context;
    onReplaceSynonyms(word);
  };

  handleClickOutside = () => {
    const { removeSynonyms } = this.context;
    removeSynonyms();
  };

  render() {
    const { synonumous } = this.props;
    const isSynonymsExist = !!synonumous.length;
    return (
      <span
        style={{
          position: "absolute",
          bottom: "100%",
          left: 0,
          display: "inline-block",
          background: "grey",
          padding: 5
        }}
      >
        {isSynonymsExist ? (
          this.props.synonumous.map(synonum => (
            <span
              key={synonum}
              style={{ padding: "0 10px", cursor: "pointer" }}
              onClick={this.onReplaceSynonymsHandler(synonum)}
            >
              {synonum}
            </span>
          ))
        ) : (
          <span>there is no any synonumous</span>
        )}
      </span>
    );
  }
}

export default onClickOutside(Poppup);
