import React, { Component } from "react";
import Poppup from "./Poppup";
import { AppContext } from "../context";

class Span extends Component {
  static contextType = AppContext;

  onDoubleClickHandler = () => {
    this.props.onWordHighlighted(this.props.index, this.props.word);
  };

  render() {
    const { word, index, style } = this.props;

    const wrapperStyles = {
      position: "relative"
    };
    const styles = {
      fontWeight: style.bold ? "bold" : null,
      fontStyle: style.italic ? "italic" : null,
      textDecoration: style.underline ? "underline" : null
    };
    const { synonumous, selectedNodeIndex } = this.context;
    const currentSynonumous = synonumous.find(synonum => index === synonum.id);
    return (
      <span onDoubleClick={this.onDoubleClickHandler} style={wrapperStyles}>
        {" "}
        <span style={styles}>{word}</span>{" "}
        {index === selectedNodeIndex && currentSynonumous ? (
          <Poppup synonumous={currentSynonumous.words} />
        ) : null}
      </span>
    );
  }
}

export default Span;
