import React, { Component } from 'react';
import './ControlPanel.css';

class ControlPanel extends Component {
    render() {
        return (
            <div id="control-panel">
                <div id="format-actions">
                    <button className="format-action" type="button" onClick={this.props.selectWordHandler("bold")}><b>B</b></button>
                    <button className="format-action" type="button" onClick={this.props.selectWordHandler("italic")}><i>I</i></button>
                    <button className="format-action" type="button" onClick={this.props.selectWordHandler("underline")}><u>U</u></button>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
