export default function (synonumous, index) {
  if (Array.isArray(synonumous) && synonumous.length) {
    return synonumous.filter(
      synonumous => synonumous.id !== index
    );
  }
}